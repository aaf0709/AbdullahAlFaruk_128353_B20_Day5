<?php

echo "<h1>implode( )</h1>";
echo "<hr>";
echo "<h3><code>Returns a string from the elements of an array.</code></h3>";
echo "<hr>";

$arr = array('Hello','World!','Beautiful','Day!');
echo implode(" ",$arr);
echo "<hr>";
echo implode(" ",$arr)."<hr>";
echo implode("+",$arr)."<hr>";
echo implode("-",$arr)."<hr>";
echo implode("X",$arr);

echo "<hr>";


?>