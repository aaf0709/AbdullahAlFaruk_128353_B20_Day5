<?php

echo "<h1>substr( )</h1>";
echo "<hr>";
echo "<h3><code>Returns a part of a string.</code></h3>";
echo "<hr>";

//$str = "WELCOME TO BITM !";

// Positive numbers:
echo substr("Hello world",0,10);
echo "<hr>";
echo substr("Hello world",1,8);
echo "<hr>";

echo substr("Hello world",0,5);
echo "<hr>";
echo substr("Hello world",6,6);
echo "<hr>";

// Negative numbers:
echo substr("Hello world",0,-1);
echo "<hr>";
echo substr("Hello world",-10,-2);
echo "<hr>";
echo substr("Hello world",0,-6);
echo "<hr>";
echo substr("Hello world",6);

echo "<hr>";

?>