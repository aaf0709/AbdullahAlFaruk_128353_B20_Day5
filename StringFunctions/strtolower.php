<?php

echo "<h1>strtolower( )</h1>";
echo "<hr>";
echo "<h3><code>Converts a string to lowercase letters.</code></h3>";
echo "<hr>";

$str = "WELCOME TO BITM !";

echo strtolower($str);
echo "<hr>";

?>