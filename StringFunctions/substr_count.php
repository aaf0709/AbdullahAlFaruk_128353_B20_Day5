<?php

echo "<h1>substr_count( )</h1>";
echo "<hr>";
echo "<h3><code>Counts the number of times a substring occurs in a string.</code></h3>";
echo "<hr>";

$str = "WELCOME TO BITM !";

echo substr_count("Hello world. The world is nice.","world");

echo "<hr>";
echo substr_count($str,"TO");
echo "<hr>";
echo substr_count($str,"TO",2);
echo "<hr>";
echo substr_count($str,"TO",3);
echo "<hr>";
echo substr_count($str,"TO",3,3);
echo "<hr>";

?>