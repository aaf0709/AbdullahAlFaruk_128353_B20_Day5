<?php

echo "<h1>strip_tags( )</h1>";
echo "<hr>";
echo "<h3><code>Strips HTML and PHP tags from a string.</code></h3>";
echo "<hr>";

//$str = "Welcome to BiTM !";

echo strip_tags("Hello <b>world!</b>");

echo "<hr>";

echo strip_tags("Hello <b><i>world!</i></b>","<b>");

echo "<hr>";


?>