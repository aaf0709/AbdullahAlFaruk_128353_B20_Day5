<?php

echo "<h1>str_shuffle( )</h1>";
echo "<hr>";
echo "<h3><code>Randomly shuffles all characters in a string.</code></h3>";
echo "<hr>";

$str = "Welcome to BiTM !";
echo str_shuffle($str);

echo "<hr>";

$numbers = range(1, 20);
shuffle($numbers);
foreach ($numbers as $number) {
    echo "$number ";
}

    echo "<hr>";

?>