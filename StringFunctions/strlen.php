<?php

echo "<h1>strlen( )</h1>";
echo "<hr>";
echo "<h3><code>Returns the length of a string.</code></h3>";
echo "<hr>";

$str = "Welcome to BiTM !";

echo strlen($str);

echo "<hr>";
echo strlen("Hello world!");

echo "<hr>";

?>