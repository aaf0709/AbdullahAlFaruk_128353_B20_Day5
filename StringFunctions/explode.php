<?php

echo "<h1>explode( )</h1>";
echo "<hr>";
echo "<h3><code>Breaks a string into an array.</code></h3>";
echo "<hr>";

echo "<pre>";
$str = "Hello world. It's a beautiful day.";
print_r (explode(" ",$str));
echo "</pre>";
echo "<hr>";

$str1 = 'one,two,three,four';

echo "<pre>";
print_r(explode(',',$str1,0));
echo "</pre>";
echo "<hr>";

echo "<pre>";
print_r(explode(',',$str1,-1));
echo "</pre>";
echo "<hr>";

echo "<pre>";
print_r(explode(',',$str1,2));
echo "</pre>";
echo "<hr>";

?>