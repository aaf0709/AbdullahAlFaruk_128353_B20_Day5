<?php

echo "<h1>printf( )</h1>";
echo "<hr>";
echo "<h3><code>Outputs a formatted string.</code></h3>";
echo "<hr>";

$num = 2;
$str = "BiTM";
printf("There are %u labs in %s Chittagong campus.",$num,$str);

echo "<hr>";

$number = 123;
printf("With 2 decimals: %1\$.2f
<br>With no decimals: %1\$u",$number);

echo "<hr>";

?>