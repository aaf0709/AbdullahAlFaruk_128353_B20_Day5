<?php

echo "<h1>str_pad( )</h1>";
echo "<hr>";
echo "<h3><code>Pads a string to a new length.</code></h3>";
echo "<hr>";

$str = "Welcome to BiTM !";

echo str_pad($str,25,'.',STR_PAD_LEFT);

echo "<hr>";

echo str_pad($str,25,'.',STR_PAD_RIGHT);

echo "<hr>";

echo str_pad($str,25,'.',STR_PAD_BOTH);

echo "<hr>";


?>