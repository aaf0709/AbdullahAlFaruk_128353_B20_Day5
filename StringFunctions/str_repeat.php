<?php

echo "<h1>str_repeat( )</h1>";
echo "<hr>";
echo "<h3><code>Repeats a string a specified number of times.</code></h3>";
echo "<hr>";

$str = " Welcome to BiTM ! ";

echo str_repeat(".=.",35);

echo "<hr>";

echo str_repeat($str,5);

echo "<hr>";


?>