<?php

echo "<h1>rtrim( )</h1>";
echo "<hr>";
echo "<h3><code>Removes whitespace or other characters from the right side of a string.</code></h3>";
echo "<hr>";

$str = "Hello World!";
echo $str . "<br>";
echo rtrim($str,"World!");
echo "<hr>";



?>