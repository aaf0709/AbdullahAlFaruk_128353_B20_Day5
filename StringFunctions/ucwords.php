<?php

echo "<h1>ucwords( )</h1>";
echo "<hr>";
echo "<h3><code>Converts the first character of each word in a string to uppercase.</code></h3>";
echo "<hr>";

$str = "welcome to bitm !";

echo ucwords($str);

echo "<hr>";
echo ucwords("bangldesh is my country.");

echo "<hr>";


?>