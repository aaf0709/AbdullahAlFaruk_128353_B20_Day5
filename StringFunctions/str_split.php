<?php

echo "<h1>str_repeat( )</h1>";
echo "<hr>";
echo "<h3><code>Splits a string into an array.</code></h3>";
echo "<hr>";

$str = "Welcome to BiTM !";

echo "<pre>";
print_r(str_split($str,8));
echo "</pre>";

echo "<hr>";
echo "<pre>";
print_r(str_split("Hello",3));
echo "</pre>";

echo "<hr>";

echo "<pre>";
print_r(str_split("Hello"));
echo "</pre>";

echo "<hr>";


?>