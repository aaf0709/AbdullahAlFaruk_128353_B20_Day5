<?php

echo "<h1>addcslashes( )</h1>";
echo "<hr>";
echo "<h3><code>Returns a string with backslashes in front of the specified characters.</code></h3>";
echo "<hr>";

$str = addcslashes("Hello World!","W");
echo($str);
echo "<hr>";

?>