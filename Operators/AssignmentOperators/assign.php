<?php

echo "<h1>Assignment Operator</h1>";
echo "<hr>";
echo "<h3><code>The basic assignment operator is '='. The left operand gets set to the value of the expression on the right.</code></h3>";
echo "<hr>";

$a = 128353;

echo $a;
echo "<hr>";
echo 'a '  .  '  =  '  .  $a;
echo "<hr>";


?>