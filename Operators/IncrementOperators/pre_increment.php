<?php

echo "<h1>Pre Increment Operator  (++a)</h1>";
echo "<hr>";
echo "<h3><code>Increments a by one, then returns a.</code></h3>";
echo "<hr>";

$a = 50;
echo ++$a;
echo "<hr>";
echo $a;
echo "<hr>";

?>