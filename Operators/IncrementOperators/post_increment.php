<?php

echo "<h1>Post Increment Operator  (a++)</h1>";
echo "<hr>";
echo "<h3><code>Returns a, then increments a by one.</code></h3>";
echo "<hr>";

$a = 50;
echo $a++;
echo "<hr>";
echo $a;
echo "<hr>";

?>