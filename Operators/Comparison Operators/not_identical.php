<?php

echo "<h1>Comparision Operator  (Not Identical !==)</h1>";
echo "<hr>";
echo "<h3><code>TRUE if a is not equal to b, or they are not of the same type.</code></h3>";
echo "<hr>";

var_dump(0 !== 1);
echo "<hr>";
var_dump(0 !== 0);
echo "<hr>";
var_dump("01" <> "101");
echo "<hr>";
var_dump("01" <> 01);
echo "<hr>";


?>