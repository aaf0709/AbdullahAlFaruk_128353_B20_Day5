<?php

echo "<h1>Comparision Operator  (Equal = =)</h1>";
echo "<hr>";
echo "<h3><code>TRUE if a is equal to b after type juggling..</code></h3>";
echo "<hr>";

var_dump(0 == "a"); // 0 == 0 -> true
echo "<hr>";
var_dump("1" == "01"); // 1 == 1 -> true
echo "<hr>";
var_dump("10" == "1e1"); // 10 == 10 -> true
echo "<hr>";
var_dump(100 == "1e2"); // 100 == 100 -> true
echo "<hr>";

?>