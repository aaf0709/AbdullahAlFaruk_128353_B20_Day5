<?php

echo "<h1>Exponentiation</h1>";
echo "<hr>";
echo "<h3><code>Result of raising a to the b'th power. Introduced in PHP 5.6.</code></h3>";
echo "<hr>";

$a = 2;
$b = 8;
$c = $a ** $b;
echo $a ** $b;
echo "<hr>";
echo $c;
echo "<hr>";
echo $a. ' ** ' .$b. ' = ' .$c;
echo "<hr>";

?>